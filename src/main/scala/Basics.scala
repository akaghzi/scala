object Basics {
  def main(args: Array[String]): Unit = {
    //    Data Types
    val myInt: Int = 143
    println(myInt)
    val appName = "First Application"
    println(appName)
    val PI = 22.0 / 7
    println("PI: " + PI)
    println(getOption())
    val someVal = 5
    val someRef = List(1, 2, 3, 4)

    //    functions
    printAny(someVal)
    printAny(someRef)
    printAnyval(someVal)
    printAnyref(someRef)

    //    string substitution
    println(s"myInt*PI is: ${myInt * PI} and defined in application $appName")

    //    expression block
    val valFromExpBlock = {
      //      last expression is returned
      myInt + 2 //ignored
      2 + 4
    }
    println(s"valFromExpBlock is: $valFromExpBlock")

    // conditional
    if(myInt <= 200){
      println("myInt less than or equal to threshold of 200")
    } else {
      println("myInt greater than threshold of 200")
    }

    val myIf = if(myInt <= 200){
      println("myInt less than or equal to threshold of 200")
      1
    } else {
      println("myInt greater than threshold of 200")
      0
    }

    println("myIf: " + myIf)

    val someIf = if(myInt > 200){
      println("myInt")
      1
    }
    println(someIf.getClass)
    println("someIf: "+someIf)

    // also called value binding
    println("for loop...")
    for(i <- someRef){
      println(s"current element is: $i")
    }

    val daysOfTheWeek = List("mon","tue","wed","thu","fri","sat","sun")
    println(daysOfTheWeek(0))

    // otherway for loop
    for (idx <- 0 to daysOfTheWeek.size - 1){
      println(daysOfTheWeek(idx))
    }

    // yet other way
    println("until...")
    for (idx <- 0 until daysOfTheWeek.size){
      println(daysOfTheWeek(idx))
    }

    // for loop with pattern guard
    println("for loop with pattern guard...")
    for (day <- daysOfTheWeek if day == "tue"){
      println(day)
    }

    val daysOfTheWeekEnd = List("sat","sun")
    // nested for loop
    println("nested for loops ...")
    for{idx <- 0 until 2
        day <- daysOfTheWeekEnd}
    {
      println(s"$idx, $day")
    }

    // use loop as expression
    val dOTW = for(day <- daysOfTheWeek) yield {
      day
    }
    println(dOTW)

    val x_dOTW = for(day <- daysOfTheWeek) yield {
      day match {
        case "mon" => "first day"
        case otherday => otherday
      }
    }
    println(x_dOTW)

    // while loop, do not return any values therefore can not be used as expressions
    println("while loop ...")
    var whileVar = 0
    while(whileVar < 6 ){
      println(s"whileVar is: $whileVar")
      whileVar += 1
    }

    // pattern matching
    println("pattern matching ...")
    val myDay = "thu"
    val typeOfMyDay = myDay match{
      case "mon" => "firstday" //straight match
      case "sat"|"sun" => "weekend :-)" // ORed match
      case otherday if otherday == "wed" => "middleday" //conditional match, otherday is stub
      case otherday => otherday //catch all others otherday here is a stub
    }
    println(typeOfMyDay)

    val dayOfWeek = "thu"
    val typeOfDay = dayOfWeek match{
      case "mon" => "firstday" //straight match
      case "sat"|"sun" => "weekend :-)" // ORed match
      case otherday if otherday == "wed" => "middleday" //conditional match, otherday is stub var
      case _ => "weekday ;-(" //catch all others
    }
    println(typeOfDay)

    // pattern matching using data type
    println("pattern matching using data type...")
    val someDay:Any = "thu" //notice that Any is a superclass of all data types
    val someDayType = someDay match{
      case someDay:Int => "Int"
      case someDay:String => "String"
      case someDay:Double => "Double"
      case _ => "SomeOtherType"
    }
    println(someDayType)


  }

  def getOption(): Int = {
    //    last expression is automatically returned
    1
  }

  def printAny(x: Any) {
    println(x)
  }

  def printAnyval(y: AnyVal) {
    println(y)
  }

  def printAnyref(z: AnyRef) {
    println(z)
  }

}