object Functions {

  def main(args: Array[String]): Unit = {
    val myRadius = getArea(10)
    println(myRadius)
    println(getArea(4))
    // Assigning method to function
    println("Assigning method to function (use with caution) ...")
    val calcCircleArea: Double => Double = getCircleArea
    println(calcCircleArea(8))
    val fCalcCircleArea = getCircleArea _
    println(fCalcCircleArea(5))
    val calcCrclArea: Double => Double = getAreaClosure
    println(calcCrclArea(2))

    println(getCircleStats(3))
    println("More function examples ...")
    println(compareStringsAscending("a", "b"))
    println(compareStringsAscending("f", "b"))
    println(compareStringsAscending("b", "b"))

    println(compareStringsDescending("a", "b"))
    println(compareStringsDescending("f", "b"))
    println(compareStringsDescending("b", "b"))
    println("Using higher order method/function ....")
    println(smartCompare("a", "b", compareStringsDescending))
    println(smartCompare("a", "b", compareStringsAscending))
    println("Higher order function with function as return variable ...")
    val compOperReverseFunc = getCompOperator(true)
    val compOperFunc = getCompOperator(false)
    println(compOperFunc("a","b"))
    println(compOperReverseFunc("a","b"))
    println("functions with default parameters ...")
    println(xGetCircleStats(r=4))
    println("functions with generics ...")
    printPairType("a",23)
    println("function with partial completion ...")
    val defaultCompareFunction = smartCompare(_:String,_:String,compareStringsAscending)
    println(defaultCompareFunction("a","b"))
    println("currying ...")
    val xDefaultCompare = curriedStringsCompare(compareStringsAscending)(_:String,_:String)
    println(xDefaultCompare("a","b"))
    println("Normal parameter ...")
    sayHelloTo("Asim")
    println("By name parameter ...")
    //no difference if a literal value is used
    sayHelloToByName("Asim")
    //difference is know when a volatile is passed as parameter
    println("Check difference when a volatile is passed ...")
    println("normal method")
    sayHelloTo(addGreeting("Asim"))
    println("named parameter")
    sayHelloToByName(addGreeting("Asim"))
    println("Tuples ...")
    val personInfo = ("Asim","Kaghzi",50,"M")
    val keyValuePair = ("Asim" -> "M")
    // tuple needs a productIterator call
    personInfo.productIterator.foreach(i => {println(s"Current value is: $i")})
    val (firstName,lastName,age,gender) = personInfo
    val (xFirstName,xLastName,_,xGender) = personInfo
    println(s"Lastname: $xLastName, gender: $xGender")
    print("Firstname: ",personInfo._1)
    println(" age: ",personInfo._3)
    println(personInfo._4)
    printPersonGender("Zarina","F")
    (printPersonGender _).tupled(keyValuePair)
    println(personInfo.productArity)
    println(personInfo.productElement(1))
    // Lists
    println("Lists ...")
    val weekDays = "mon"::"tue"::"wed"::"thu"::"fri"::Nil
    println(weekDays)
    val weekendDays = List("sat","sun")
    println(weekendDays)
    // list concatenation
    println("Lists' concatenation ...")
    val allWeekDays = weekDays:::weekendDays
    println(allWeekDays)
    val xAllWeekDays = weekDays++weekendDays
    println(xAllWeekDays)
    val yAllWeekDays = List(weekDays,weekendDays).flatten
    println(yAllWeekDays)
    val oddNumbers = List(1,3,5,7,9)
    val evenNumbers = List(2,4,6,8)
    //note 9 from first list is dropped
    val allNumbers = oddNumbers zip evenNumbers
    println(allNumbers)
    // various methods and operations on lists
    println(oddNumbers.head)
    println(evenNumbers.tail)
    println(allWeekDays(3))
    println(weekDays.contains("sat"))
    for (i <- weekendDays) {println(i)}
    val notMondayDays = allWeekDays forall(_!="Monday")
    println(notMondayDays)
    println(allWeekDays endsWith weekendDays)
    println(allWeekDays endsWith(weekendDays))
    println(allWeekDays.endsWith(weekendDays))
    println(oddNumbers.endsWith(List(10)))
    // note the 3rd entry
    val provinceCode = Map("AB" -> "Alberta",
                           "BC" -> "British Columbia",
                           ("NB" , "New Brunswick")
    )
    println(provinceCode)
    println(provinceCode("AB"))
    println(provinceCode("NB"))
    provinceCode.foreach((p:(String,String)) => println(p._1+"="+p._2))
    val provinceCodes = List("AB","ON","QC")
    val provinceNames = List("Alberta","Ontario","Quebec")
    val provinceCodeAndNamesMap = (provinceCodes zip provinceNames).toMap
    println(provinceCodeAndNamesMap("QC"))
    val provCodes = provinceCodeAndNamesMap.keys.toList
    println(provCodes)
    val provNames = provinceCodeAndNamesMap.values.toList
    println(provNames)
    //Exception handling
    println("Exception handling ...")
    println(getFraction(22.0,7))
    println(getFraction(22.0,0))
    println(getFraction(22.0,0) getOrElse("Something went wrong"))
    val fraction = getFraction(22,7) match {
      case Some(pi) => pi
      case None => "Something did not go right this time around"
    }
    println(fraction)

    val britishColumbia = util.Try(provinceCodeAndNamesMap("BC"))
    println(britishColumbia)





  }

  // function definition
  val getArea = (radius: Double) => {
    val PI = 22.0 / 7
    radius * radius * PI
  }: Double

  def getCircleArea(r: Double): Double = {
    val PI = 22.0 / 7
    r * r * PI
  }

  def getAreaClosure = {
    val PI = 22.0 / 7
    val getCrclArea = (r: Double) => {
      r * r * PI
    }: Double
    getCrclArea
  }

  def getCircleStats(r: Double) = {
    val PI = 22.0 / 7

    def getCircumference(r: Double) = {
      2 * PI * r
    }

    def getArea(r: Double) = {
      r * r * PI
    }

    (getCircumference(r), getArea(r))
  }

  def compareStringsAscending(s1: String, s2: String) = {
    if (s1 < s2) {
      -1
    } else if (s1 > s2) {
      1
    } else {
      0
    }
  }

  def compareStringsDescending(s1: String, s2: String) = {
    if (s1 < s2) {
      1
    } else if (s1 > s2) {
      -1
    } else {
      0
    }
  }

  // here stringCompare if replacement of function name
  def smartCompare(s1: String, s2: String, stringCompare: (String, String) => Int): Int = {
    stringCompare(s1, s2)
  }

  def getCompOperator(reverse: Boolean): (String, String) => Int = {
    if (reverse) {
      compareStringsDescending
    } else {
      compareStringsAscending
    }
  }

  def xGetCircleStats(PI:Double=22.0/7,r: Double) = {

    def getCircumference(r: Double) = {
      2 * PI * r
    }

    def getArea(r: Double) = {
      r * r * PI
    }

    (getCircumference(r), getArea(r))
  }

  def printPairType[K,V](k:K,v:V)={
    val kType = k.getClass
    val vType = v.getClass
    println(s"$k is of type: $kType and $v is of type: $vType")
  }
  // parameter groups
  def curriedStringsCompare(compareStrings:(String,String)=>Int)
                           (s1:String, s2:String):Int = {
    compareStrings(s1,s2)
  }

  //by name parameters
  def sayHelloTo(name:String):String={
    println(s"Hello $name")
    println(s"Hello again, $name")
    name
  }

  def addGreeting(n:String):String = {
    println("Adding greeting")
    s"Dear $n"
  }

  def sayHelloToByName(name: => String):String={
    println(s"Hello $name")
    println(s"Hello again, $name")
    name
  }

  def printPersonGender(name:String,gender:String) = {
    println(s"name: $name, gender[M/F]: $gender")
  }

  def getFraction(n1:Double,d1:Int):Option[Double] = {
    if(d1==0)
      None
    else
      Option(n1/d1)
  }

}